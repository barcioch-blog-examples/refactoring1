import { MatchScoreParser } from './solution/score-formatter/match-score-parser';
import { SingleMatchScoreFormatter } from './solution/score-formatter/single-match-score-formatter';
import { MultiQuarterScoreFormatter } from './solution/score-formatter/multi-quarter-score-formatter';
import { MultiSetScoreFormatter } from './solution/score-formatter/multi-set-score-formatter';
import { EventScoreFormatter } from './solution/score-formatter/event-score-formatter';
import { VsSeparatedNameMaker } from './solution/name-maker/vs-separated-name-makers';
import { DashSeparatedNameMaker } from './solution/name-maker/dash-separated-name-maker';
import { EventNameMaker } from './solution/name-maker/event-name-maker';
import { EventParser } from './solution/event-parser';


export const matches = [
  {
    sport: 'soccer',
    participant1: 'Chelsea',
    participant2: 'Arsenal',
    score: '2:1',
  },
  {
    sport: 'volleyball',
    participant1: 'Germany',
    participant2: 'France',
    score: '3:0,25:23,25:19,25:21',
  },
  {
    sport: 'handball',
    participant1: 'Pogoń Szczeciń',
    participant2: 'Azoty Puławy',
    score: '34:26',
  },
  {
    sport: 'basketball',
    participant1: 'GKS Tychy',
    participant2: 'GKS Katowice',
    score: [
      ['9:7', '2:1'],
      ['5:3', '9:9'],
    ],
  },
  {
    sport: 'tennis',
    participant1: 'Maria Sharapova',
    participant2: 'Serena Williams',
    score: '2:1,7:6,6:3,6:7',
  },
  {
    sport: 'soccer',
    // missing score
    participant1: 'Chelsea',
    participant2: 'Arsenal',
  },
  {
    // missing sport name
    score: '2:3',
    participant1: 'Chelsea',
    participant2: 'Arsenal',
  },
  {
    sport: 'tennis',
    score: '2:1,7:6,6:3,6:7',
    participant1: 'Maria Sharapova',
    // missing participant2
  },
  {
    sport: 'tennis',
    score: '2:1,7:6,6:3,6:7',
    // missing participant1
    participant2: 'Serena Williams',
  },
  {
    sport: 'hockey', // not existing sport
    participant1: 'Maria Sharapova',
    participant2: 'Serena Williams',
    score: '2:1,7:6,6:3,6:7',
  },
  {
    sport: 'tennis',
    participant1: 'Maria Sharapova',
    participant2: 'Serena Williams',
    score: '0-1', // invalid score format
  },
];

const matchScoreParser = new MatchScoreParser();
const singleMatchScoreFormatter = new SingleMatchScoreFormatter(matchScoreParser);
const multiQuarterScoreFormatter = new MultiQuarterScoreFormatter(singleMatchScoreFormatter);
const multiSetScoreFormatter = new MultiSetScoreFormatter(singleMatchScoreFormatter);
const eventScoreFormatter = new EventScoreFormatter(
  multiQuarterScoreFormatter,
  singleMatchScoreFormatter,
  multiSetScoreFormatter
);

const vsSeparatedNameMaker = new VsSeparatedNameMaker();
const dashSeparatedNameMaker = new DashSeparatedNameMaker();
const eventNameMaker = new EventNameMaker(vsSeparatedNameMaker, dashSeparatedNameMaker);

const eventParser = new EventParser(eventNameMaker, eventScoreFormatter);


const parsedMatches = matches.map(match => {
  try {
    return eventParser.parse(match);
  } catch (error) {
    // ignore error
  }
}).filter(v => v != null);

console.log(parsedMatches);

