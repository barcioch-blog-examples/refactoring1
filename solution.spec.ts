import { EventParser } from './solution/event-parser';
import { Match } from './solution/models/match';
import { UnsupportedSportException } from './solution/exceptions/unsupported-sport.exception';
import { InvalidScoreException } from './solution/exceptions/invalid-score.exception';
import { InvalidScoreFormatException } from './solution/exceptions/invalid-score-format.exception';
import { InvalidParticipantNameException } from './solution/exceptions/invalid-participant-name.exception';
import { SportEvent } from './solution/models/sport-event';
import { MatchScoreParser } from './solution/score-formatter/match-score-parser';
import { SingleMatchScoreFormatter } from './solution/score-formatter/single-match-score-formatter';
import { MultiQuarterScoreFormatter } from './solution/score-formatter/multi-quarter-score-formatter';
import { MultiSetScoreFormatter } from './solution/score-formatter/multi-set-score-formatter';
import { EventScoreFormatter } from './solution/score-formatter/event-score-formatter';
import { VsSeparatedNameMaker } from './solution/name-maker/vs-separated-name-makers';
import { DashSeparatedNameMaker } from './solution/name-maker/dash-separated-name-maker';
import { EventNameMaker } from './solution/name-maker/event-name-maker';


describe('when EventParser is created', () => {
  let eventParser: EventParser;

  beforeAll(() => {
    eventParser = getEventParser();
  });

  it('should throw UnsupportedSportException when "sport" propery is missing', () => {
    const input: Partial<Match> = {
      score: '2:3',
      participant1: 'Chelsea',
      participant2: 'Arsenal',
    };
    const expected = new UnsupportedSportException();
    expect(() => eventParser.parse(input)).toThrow(expected);
  });

  it('should throw UnsupportedSportException when "sport" propery is set but not recognized', () => {
    const input: Partial<Match> = {
      sport: 'hockey',
      score: '2:3',
      participant1: 'Chelsea',
      participant2: 'Arsenal',
    };
    const expected = new UnsupportedSportException();
    expect(() => eventParser.parse(input)).toThrow(expected);
  });

  it('should throw InvalidScoreException when "score" propery is missing', () => {
    const input: Partial<Match> = {
      sport: 'soccer',
      participant1: 'Chelsea',
      participant2: 'Arsenal',
    };
    const expected = new InvalidScoreException();
    expect(() => eventParser.parse(input)).toThrow(expected);
  });

  it('should throw InvalidScoreFormatException when "score" propery is missing', () => {
    const input: Partial<Match> = {
      sport: 'soccer',
      score: '3-3',
      participant1: 'Chelsea',
      participant2: 'Arsenal',
    };
    const expected = new InvalidScoreFormatException();
    expect(() => eventParser.parse(input)).toThrowError(expected);
  });

  it('should throw InvalidParticipantNameException when "participant1" propery is missing', () => {
    const input: Partial<Match> = {
      sport: 'tennis',
      score: '2:1,7:6,6:3,6:7',
      participant2: 'Serena Williams',
    };
    const expected = new InvalidParticipantNameException();
    expect(() => eventParser.parse(input)).toThrowError(expected);
  });

  it('should throw InvalidParticipantNameException when "participant1" propery is an empty string', () => {
    const input: Partial<Match> = {
      sport: 'tennis',
      score: '2:1,7:6,6:3,6:7',
      participant1: '   ',
      participant2: 'Serena Williams',
    };
    const expected = new InvalidParticipantNameException();
    expect(() => eventParser.parse(input)).toThrowError(expected);
  });

  it('should throw InvalidParticipantNameException when "participant2" propery is missing', () => {
    const input: Partial<Match> = {
      sport: 'tennis',
      score: '2:1,7:6,6:3,6:7',
      participant1: 'Serena Williams',
    };
    const expected = new InvalidParticipantNameException();
    expect(() => eventParser.parse(input)).toThrowError(expected);
  });

  it('should throw InvalidParticipantNameException when "participant2" propery is an empty string', () => {
    const input: Partial<Match> = {
      sport: 'tennis',
      score: '2:1,7:6,6:3,6:7',
      participant2: '',
      participant1: 'Serena Williams',
    };
    const expected = new InvalidParticipantNameException();
    expect(() => eventParser.parse(input)).toThrowError(expected);
  });

  it('should throw InvalidScoreFormatException when "score" property contains invalid format', () => {
    const input: Partial<Match> = {
      sport: 'tennis',
      score: '2:1',
      participant2: 'Maria Sharapova',
      participant1: 'Serena Williams',
    };
    const expected = new InvalidScoreFormatException();
    expect(() => eventParser.parse(input)).toThrowError(expected);
  });

  it('should throw InvalidScoreFormatException when "score" property contains invalid format', () => {
    const input: Partial<Match> = {
      sport: 'basketball',
      score: [['0:1']],
      participant2: 'Maria Sharapova',
      participant1: 'Serena Williams',
    };
    const expected = new InvalidScoreFormatException();
    expect(() => eventParser.parse(input)).toThrowError(expected);
  });

  it('should throw InvalidScoreFormatException when "score" property contains invalid format', () => {
    const input: Partial<Match> = {
      sport: 'soccer',
      score: '1,2',
      participant2: 'Maria Sharapova',
      participant1: 'Serena Williams',
    };
    const expected = new InvalidScoreFormatException();
    expect(() => eventParser.parse(input)).toThrowError(expected);
  });

  it('should throw InvalidScoreFormatException when "score" property contains invalid format', () => {
    const input: Partial<Match> = {
      sport: 'handball',
      score: '1|2',
      participant2: 'Maria Sharapova',
      participant1: 'Serena Williams',
    };
    const expected = new InvalidScoreFormatException();
    expect(() => eventParser.parse(input)).toThrowError(expected);
  });

  it('should throw InvalidScoreFormatException when "score" property contains invalid format', () => {
    const input: Partial<Match> = {
      sport: 'volleyball',
      score: '12:2, 24:20',
      participant2: 'Maria Sharapova',
      participant1: 'Serena Williams',
    };
    const expected = new InvalidScoreFormatException();
    expect(() => eventParser.parse(input)).toThrowError(expected);
  });

  it('should return valid sport event when valid data is passed', () => {
    const inputMatches: Partial<Match>[] = [
      {
        sport: 'soccer',
        participant1: 'Chelsea',
        participant2: 'Arsenal',
        score: '2:1',
      },
      {
        sport: 'volleyball',
        participant1: 'Germany',
        participant2: 'France',
        score: '3:0,25:23,25:19,25:21',
      },
      {
        sport: 'handball',
        participant1: 'Pogoń Szczeciń',
        participant2: 'Azoty Puławy',
        score: '34:26',
      },
      {
        sport: 'basketball',
        participant1: 'GKS Tychy',
        participant2: 'GKS Katowice',
        score: [
          ['9:7', '2:1'],
          ['5:3', '9:9'],
        ],
      },
      {
        sport: 'tennis',
        participant1: 'Maria Sharapova',
        participant2: 'Serena Williams',
        score: '2:1,7:6,6:3,6:7',
      },
    ];

    const expected: SportEvent[] = [
      {name: 'Chelsea - Arsenal', score: '2:1'},
      {
        name: 'Germany - France',
        score: 'Main score: 3:0 (set1 25:23, set2 25:19, set3 25:21)'
      },
      {name: 'Pogoń Szczeciń vs Azoty Puławy', score: '34:26'},
      {name: 'GKS Tychy - GKS Katowice', score: '9:7,2:1,5:3,9:9'},
      {
        name: 'Maria Sharapova vs Serena Williams',
        score: 'Main score: 2:1 (set1 7:6, set2 6:3, set3 6:7)'
      }
    ];
    const parsedMatches = inputMatches.map(m => eventParser.parse(m));
    expect(parsedMatches).toEqual(expected);
  });

});

const getEventParser = (): EventParser => {
  const matchScoreParser = new MatchScoreParser();
  const singleMatchScoreFormatter = new SingleMatchScoreFormatter(matchScoreParser);
  const multiQuarterScoreFormatter = new MultiQuarterScoreFormatter(singleMatchScoreFormatter);
  const multiSetScoreFormatter = new MultiSetScoreFormatter(singleMatchScoreFormatter);
  const eventScoreFormatter = new EventScoreFormatter(
    multiQuarterScoreFormatter,
    singleMatchScoreFormatter,
    multiSetScoreFormatter
  );

  const vsSeparatedNameMaker = new VsSeparatedNameMaker();
  const dashSeparatedNameMaker = new DashSeparatedNameMaker();
  const eventNameMaker = new EventNameMaker(vsSeparatedNameMaker, dashSeparatedNameMaker);

  return new EventParser(eventNameMaker, eventScoreFormatter);
}
