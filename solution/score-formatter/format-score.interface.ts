export interface FormatScoreInterface {
  formatScore(score: unknown): string;
}
