import { InvalidScoreFormatException } from '../exceptions/invalid-score-format.exception';
import { FormatScoreInterface } from './format-score.interface';
import { SingleMatchScoreFormatter } from './single-match-score-formatter';

export class MultiQuarterScoreFormatter implements FormatScoreInterface {
  constructor(
    private readonly singleMatchScoreFormatter: SingleMatchScoreFormatter
  ) {
  }

  formatScore(score: unknown): string {
    this.validateScore(score);
    const matchScores = this.extractMatchScores(score as string[][]);

    return matchScores.join(',');
  }

  private extractMatchScores(score: string[][]): string[] {
    return score.reduce((previous, current) => previous.concat(current), [])
      .map(mr => this.singleMatchScoreFormatter.formatScore(mr));
  }

  private validateScore(score: unknown): void {
    if (!(score instanceof Array)) {
      throw new InvalidScoreFormatException();
    }

    if (score.length !== 2) {
      throw new InvalidScoreFormatException();
    }

    score.forEach(s => {
      if (!(s instanceof Array)) {
        throw new InvalidScoreFormatException();
      }
    });
  }
}
