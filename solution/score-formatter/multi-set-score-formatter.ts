import { InvalidScoreFormatException } from '../exceptions/invalid-score-format.exception';
import { FormatScoreInterface } from './format-score.interface';
import { SingleMatchScoreFormatter } from './single-match-score-formatter';

export class MultiSetScoreFormatter implements FormatScoreInterface {
  constructor(
    private readonly singleMatchScoreFormatter: SingleMatchScoreFormatter
  ) {
  }

  formatScore(score: unknown): string {
    this.validateScore(score);
    const matchScores = this.extractMatchScores(score as string);
    const mainScore = matchScores.shift();
    const formattedMatchScores = matchScores.map((matchScore, index) => {
      return `set${ index + 1 } ${ matchScore }`;
    }).join(', ');

    return `Main score: ${ mainScore } (${ formattedMatchScores })`;
  }

  private extractMatchScores(score: string): string[] {
    const matchResults = score.split(',');
    if (matchResults.length !== 4) {
      throw new InvalidScoreFormatException();
    }

    return matchResults.map(mr => this.singleMatchScoreFormatter.formatScore(mr));
  }

  private validateScore(score: unknown): void {
    if (typeof score !== 'string') {
      throw new InvalidScoreFormatException();
    }
  }
}
