import { Match } from '../models/match';
import { Sport } from '../models/sport';
import { UnsupportedSportException } from '../exceptions/unsupported-sport.exception';
import { InvalidScoreException } from '../exceptions/invalid-score.exception';
import { FormatScoreInterface } from './format-score.interface';

export class EventScoreFormatter {
  constructor(
    private readonly multiQuarterScoreFormatter: FormatScoreInterface,
    private readonly singleMatchScoreFormatter: FormatScoreInterface,
    private readonly multiSetScoreFormatter: FormatScoreInterface,
  ) {
  }

  formatScore(match: Partial<Match>): string {
    this.validateScore(match.score);

    switch (match.sport) {
      case Sport.basketball:
        return this.multiQuarterScoreFormatter.formatScore(match.score);
      case Sport.tennis:
      case Sport.volleyball:
        return this.multiSetScoreFormatter.formatScore(match.score);
      case Sport.handball:
      case Sport.soccer:
        return this.singleMatchScoreFormatter.formatScore(match.score);
      default:
        throw new UnsupportedSportException();
    }
  }

  private validateScore(score: unknown): void {
    if (score == null) {
      throw new InvalidScoreException();
    }
  }
}
