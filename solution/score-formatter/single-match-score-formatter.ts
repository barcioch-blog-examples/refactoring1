import { FormatScoreInterface } from './format-score.interface';
import { MatchScoreParser } from './match-score-parser';
import { InvalidScoreFormatException } from '../exceptions/invalid-score-format.exception';

export class SingleMatchScoreFormatter implements FormatScoreInterface {
  constructor(
    private readonly matchScoreParser: MatchScoreParser
  ) {
  }

  formatScore(score: unknown): string {
    if (typeof score !== 'string') {
      throw new InvalidScoreFormatException();
    }

    const matchScore = this.matchScoreParser.parse(score);

    return `${ matchScore.participant1Score }:${ matchScore.participant2Score }`;
  }
}
