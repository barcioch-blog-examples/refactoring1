import { MatchScore } from '../models/match-score';
import { InvalidScoreFormatException } from '../exceptions/invalid-score-format.exception';

export class MatchScoreParser {
  private readonly scoreRegexp = new RegExp('^([0-9]+):([0-9]+)$');

  parse(score: string): MatchScore {
    const regexpMatch = this.scoreRegexp.exec(score);
    if (!regexpMatch) {
      throw new InvalidScoreFormatException();
    }

    return {
      participant1Score: Number.parseInt(regexpMatch[1]),
      participant2Score: Number.parseInt(regexpMatch[2]),
    }
  }
}
