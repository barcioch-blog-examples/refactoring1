import { MakeNameInterface } from './make-name.interface';
import { Match } from '../models/match';
import { Sport } from '../models/sport';
import { UnsupportedSportException } from '../exceptions/unsupported-sport.exception';
import { InvalidParticipantNameException } from '../exceptions/invalid-participant-name.exception';

export class EventNameMaker {
  constructor(
    private readonly vsSeparatedNameMaker: MakeNameInterface,
    private readonly dashSeparatedNameMaker: MakeNameInterface,
  ) {
  }

  makeName(match: Partial<Match>): string {
    this.validateParticipant(match.participant1);
    this.validateParticipant(match.participant2);

    switch (match.sport) {
      case Sport.soccer:
      case Sport.volleyball:
      case Sport.basketball: {
        return this.dashSeparatedNameMaker.makeName(match.participant1, match.participant2);
      }
      case Sport.handball:
      case Sport.tennis: {
        return this.vsSeparatedNameMaker.makeName(match.participant1, match.participant2);
      }
      default:
        throw new UnsupportedSportException();
    }
  }

  private validateParticipant(name: string | undefined): void {
    if (typeof name !== 'string') {
      throw new InvalidParticipantNameException();
    }

    if (name.trim().length === 0) {
      throw new InvalidParticipantNameException();
    }
  }
}
