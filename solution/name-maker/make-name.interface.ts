export interface MakeNameInterface {
  makeName(participant1: string, participant2: string): string;
}
