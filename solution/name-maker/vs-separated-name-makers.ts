import { MakeNameInterface } from './make-name.interface';

export class VsSeparatedNameMaker implements MakeNameInterface {
  makeName(participant1: string, participant2: string): string {
    return `${ participant1 } vs ${ participant2 }`;
  }
}
