import { EventParserException } from './event-parser.exception';

export class InvalidScoreFormatException extends EventParserException {
  message = 'Invalid score format';
}
