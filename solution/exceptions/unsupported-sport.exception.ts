import { EventParserException } from './event-parser.exception';

export class UnsupportedSportException extends EventParserException {
  message = 'Unsupported sport';
}
