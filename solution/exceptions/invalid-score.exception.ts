import { EventParserException } from './event-parser.exception';

export class InvalidScoreException extends EventParserException {
  message = 'Invalid score';
}
