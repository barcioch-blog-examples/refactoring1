import { EventParserException } from './event-parser.exception';

export class InvalidParticipantNameException extends EventParserException {
  message = 'Invalid participant name';
}
