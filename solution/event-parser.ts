import { EventNameMaker } from './name-maker/event-name-maker';
import { EventScoreFormatter } from './score-formatter/event-score-formatter';
import { Match } from './models/match';
import { SportEvent } from './models/sport-event';

export class EventParser {
  constructor(
    private readonly eventNameMaker: EventNameMaker,
    private readonly eventScoreFormatter: EventScoreFormatter
  ) {
  }

  parse(match: Partial<Match>): SportEvent {
    return {
      name: this.eventNameMaker.makeName(match),
      score: this.eventScoreFormatter.formatScore(match)
    }
  }
}
