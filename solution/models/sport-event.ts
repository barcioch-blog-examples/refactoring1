export interface SportEvent {
  name: string;
  score: string;
}
