export enum Sport {
  soccer = 'soccer',
  volleyball = 'volleyball',
  handball = 'handball',
  basketball = 'basketball',
  tennis = 'tennis',
}
