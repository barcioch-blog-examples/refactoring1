export interface MatchScore {
  participant1Score: number;
  participant2Score: number;
}
